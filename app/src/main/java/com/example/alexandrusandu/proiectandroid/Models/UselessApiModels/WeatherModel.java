package com.example.alexandrusandu.proiectandroid.Models.UselessApiModels;

public class WeatherModel {
    private String temp;
    private String temp_min;
    private String temp_max;
    private String pressure;
    private String humidity;
    private String weatherType;
    private String windSpeed;

    public WeatherModel(String temp, String temp_min, String temp_max, String pressure, String humidity, String weatherType, String windSpeed) {
        this.temp = temp;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
        this.pressure = pressure;
        this.humidity = humidity;
        this.weatherType = weatherType;
        this.windSpeed = windSpeed;
    }

    public String getTemp() {
        return temp;
    }

    public String getTemp_min() {
        return temp_min;
    }

    public String getPressure() {
        return pressure;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getWeatherType() {
        return weatherType;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public String getTemp_max() {
        return temp_max;
    }
}
