package com.example.alexandrusandu.proiectandroid.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.alexandrusandu.proiectandroid.Adapters.TestsRecyclerAdapter;
import com.example.alexandrusandu.proiectandroid.Adapters.TestsRecyclerAdapterForManage;
import com.example.alexandrusandu.proiectandroid.Adapters.TestsRecyclerAdapterForManageForStudent;
import com.example.alexandrusandu.proiectandroid.Database.AppDatabase;
import com.example.alexandrusandu.proiectandroid.Database.Models.Test;
import com.example.alexandrusandu.proiectandroid.Database.Models.TestTaken;
import com.example.alexandrusandu.proiectandroid.Database.Models.User;
import com.example.alexandrusandu.proiectandroid.Models.TestModel;
import com.example.alexandrusandu.proiectandroid.R;
import com.example.alexandrusandu.proiectandroid.ui.common.BaseActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class ManageTestsActivity extends AppCompatActivity {
    private User user;
    private List<Test> tests=new ArrayList<>();
    private List<TestTaken> testsTaken=new ArrayList<>();
    private List<Dialog> dialogs=new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_tests);
        getUserFromDb();
    }

    private void getUserFromDb() {
        user=AppDatabase.getDatabase(ManageTestsActivity.this).userDao().getAllUsers().get(0);
        if(user.type.equals("0")){
            setViewsForTeacher();
        }else{
            setViewsForStudent();
        }
    }

    private void setViewsForStudent() {
        testsTaken = new ArrayList<>(AppDatabase.getDatabase(ManageTestsActivity.this).testDao().getAllTestsForUser(user.id));
        TestsRecyclerAdapterForManageForStudent testsRecyclerAdapterForManageForStudent = new TestsRecyclerAdapterForManageForStudent(ManageTestsActivity.this, testsTaken, user);
        ((RecyclerView) findViewById(R.id.tests_recycler)).setAdapter(testsRecyclerAdapterForManageForStudent);
        ((RecyclerView) findViewById(R.id.tests_recycler)).setLayoutManager(new LinearLayoutManager(ManageTestsActivity.this));
    }

    private void setViewsForTeacher() {
        tests = new ArrayList<Test>(AppDatabase.getDatabase(ManageTestsActivity.this).testCreatedDao().getAllTestsCreated());
        TestsRecyclerAdapterForManage testsRecyclerAdapterForManage = new TestsRecyclerAdapterForManage(ManageTestsActivity.this, tests);
        ((RecyclerView) findViewById(R.id.tests_recycler)).setAdapter(testsRecyclerAdapterForManage);
        ((RecyclerView) findViewById(R.id.tests_recycler)).setLayoutManager(new LinearLayoutManager(ManageTestsActivity.this));
    }

    public void deleteTest(final int position) {
        showDialogMessage(true, "Are you sure you want to delete the test?", "Yes", "No", new BaseActivity.OnDialogButtonsPressed() {
            @Override
            public void onPositiveButtonPressed() {
                deleteTestFromDatabases(position);
            }

            @Override
            public void onNegativeButtonPressed() {

            }
        });
    }

    private void deleteTestFromDatabases(final int position) {


        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("tests")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            TestModel testModel = new TestModel(document);
                            if(testModel.createdBy.equals(tests.get(position).createdBy)&&testModel.nume.equals(tests.get(position).nume)){
                                String docToDelete=document.getId();
                                db.collection("tests").document(docToDelete).delete()
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                AppDatabase.getDatabase(ManageTestsActivity.this).questionDao().deleteForTest(tests.get(position).id);
                                                AppDatabase.getDatabase(ManageTestsActivity.this).testCreatedDao().deleteTest(tests.get(position).id);

                                                setViewsForTeacher();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {

                                            }
                                        });
                            }
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ManageTestsActivity.this, "Cannot load tests", Toast.LENGTH_LONG).show();
                    }
                });
    }


    public interface OnDialogButtonsPressed {
        void onPositiveButtonPressed();

        void onNegativeButtonPressed();
    }


    public void showDialogMessage(Boolean cancelable, String message, String optionPositive, String optionNegative, final BaseActivity.OnDialogButtonsPressed onDialogButtonsPressed) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        if (onDialogButtonsPressed != null)
                            onDialogButtonsPressed.onPositiveButtonPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        if (onDialogButtonsPressed != null)
                            onDialogButtonsPressed.onNegativeButtonPressed();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog dialog;
        builder.setCancelable(cancelable);
        if (optionNegative == null)
            builder.setMessage(message).setPositiveButton(optionPositive, dialogClickListener);

        if (optionPositive == null)
            builder.setMessage(message).setNegativeButton(optionNegative, dialogClickListener);

        if (optionNegative != null && optionPositive != null)
            builder.setMessage(message).setNegativeButton(optionNegative, dialogClickListener)
                    .setPositiveButton(optionPositive, dialogClickListener);

        if (!this.isFinishing()) {
            dialog = builder.show();
            dialogs.add(dialog);
        }

    }
}
