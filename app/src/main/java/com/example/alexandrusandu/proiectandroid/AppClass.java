package com.example.alexandrusandu.proiectandroid;

import android.app.Application;

import com.example.alexandrusandu.proiectandroid.Database.AppDatabase;

public class AppClass extends Application {
    static public AppDatabase database;
    @Override
    public void onCreate() {
        super.onCreate();
        database=AppDatabase.getDatabase(getApplicationContext());
    }
}
