package com.example.alexandrusandu.proiectandroid.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.alexandrusandu.proiectandroid.Database.AppDatabase;
import com.example.alexandrusandu.proiectandroid.Database.Models.Question;
import com.example.alexandrusandu.proiectandroid.Database.Models.Test;
import com.example.alexandrusandu.proiectandroid.Database.Models.TestTaken;
import com.example.alexandrusandu.proiectandroid.ui.common.BaseActivity;
import com.example.alexandrusandu.proiectandroid.Models.QuestionsModel;
import com.example.alexandrusandu.proiectandroid.Models.TestModel;
import com.example.alexandrusandu.proiectandroid.R;
import com.example.alexandrusandu.proiectandroid.Utils.Constants;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateActivity extends BaseActivity {
    private TestModel testModel = new TestModel(new ArrayList<QuestionsModel>(), "", "", "", "");
    private EditText testName;
    private EditText question1;
    private EditText question2;
    private EditText question3;
    private EditText question4;
    private EditText question5;
    private EditText ans1quest1;
    private EditText ans2quest1;
    private EditText ans3quest1;
    private EditText ans4quest1;
    private EditText ans1quest2;
    private EditText ans2quest2;
    private EditText ans3quest2;
    private EditText ans4quest2;
    private EditText ans1quest3;
    private EditText ans2quest3;
    private EditText ans3quest3;
    private EditText ans4quest3;
    private EditText ans1quest4;
    private EditText ans2quest4;
    private EditText ans3quest4;
    private EditText ans4quest4;
    private EditText ans1quest5;
    private EditText ans2quest5;
    private EditText ans3quest5;
    private EditText ans4quest5;

    private Spinner spinner1;
    private Spinner spinner2;
    private Spinner spinner3;
    private Spinner spinner4;
    private Spinner spinner5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_create);
        super.onCreate(savedInstanceState);
        setViews();
        getViewReferences();
        setControls();
    }

    private void getViewReferences() {
        testName = ((EditText) findViewById(R.id.et_name));
        question1 = ((EditText) findViewById(R.id.et_question1));
        question2 = ((EditText) findViewById(R.id.et_question2));
        question3 = ((EditText) findViewById(R.id.et_question3));
        question4 = ((EditText) findViewById(R.id.et_question4));
        question5 = ((EditText) findViewById(R.id.et_question5));
        ans1quest1 = ((EditText) findViewById(R.id.et_ans1quest1));
        ans2quest1 = ((EditText) findViewById(R.id.et_ans2quest1));
        ans3quest1 = ((EditText) findViewById(R.id.et_ans3quest1));
        ans4quest1 = ((EditText) findViewById(R.id.et_ans4quest1));
        ans1quest2 = ((EditText) findViewById(R.id.et_ans1quest2));
        ans2quest2 = ((EditText) findViewById(R.id.et_ans2quest2));
        ans3quest2 = ((EditText) findViewById(R.id.et_ans3quest2));
        ans4quest2 = ((EditText) findViewById(R.id.et_ans4quest2));
        ans1quest3 = ((EditText) findViewById(R.id.et_ans1quest3));
        ans2quest3 = ((EditText) findViewById(R.id.et_ans2quest3));
        ans3quest3 = ((EditText) findViewById(R.id.et_ans3quest3));
        ans4quest3 = ((EditText) findViewById(R.id.et_ans4quest3));
        ans1quest4 = ((EditText) findViewById(R.id.et_ans1quest4));
        ans2quest4 = ((EditText) findViewById(R.id.et_ans2quest4));
        ans3quest4 = ((EditText) findViewById(R.id.et_ans3quest4));
        ans4quest4 = ((EditText) findViewById(R.id.et_ans4quest4));
        ans1quest5 = ((EditText) findViewById(R.id.et_ans1quest5));
        ans2quest5 = ((EditText) findViewById(R.id.et_ans2quest5));
        ans3quest5 = ((EditText) findViewById(R.id.et_ans3quest5));
        ans4quest5 = ((EditText) findViewById(R.id.et_ans4quest5));

        spinner1 = (Spinner) findViewById(R.id.sp_question1);
        spinner2 = (Spinner) findViewById(R.id.sp_question2);
        spinner3 = (Spinner) findViewById(R.id.sp_question3);
        spinner4 = (Spinner) findViewById(R.id.sp_question4);
        spinner5 = (Spinner) findViewById(R.id.sp_question5);
    }

    private void setControls() {
        findViewById(R.id.btn_send_test).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAllValuesFromAllStringsAndGoCrazy();
            }
        });
    }

    private void getAllValuesFromAllStringsAndGoCrazy() {
        if (testName.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }

        if (question1.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (question2.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (question3.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (question4.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (question5.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans1quest1.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans2quest1.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans3quest1.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans4quest1.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans1quest2.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans2quest2.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans3quest2.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans4quest2.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans1quest3.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans2quest3.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans3quest3.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans4quest3.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans1quest4.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans2quest4.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans3quest4.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans4quest4.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans1quest5.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans2quest5.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans3quest5.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        if (ans4quest5.getText().toString().equals("")) {
            Toast.makeText(CreateActivity.this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }


        ArrayList<QuestionsModel> questionsModels = new ArrayList<>();
        questionsModels.add(new QuestionsModel(question1.getText().toString(), ans1quest1.getText().toString(), ans2quest1.getText().toString(), ans3quest1.getText().toString(), ans4quest1.getText().toString(), (spinner1.getSelectedItemPosition() + 1) + ""));
        questionsModels.add(new QuestionsModel(question2.getText().toString(), ans1quest2.getText().toString(), ans2quest2.getText().toString(), ans3quest2.getText().toString(), ans4quest2.getText().toString(), (spinner2.getSelectedItemPosition() + 1) + ""));
        questionsModels.add(new QuestionsModel(question3.getText().toString(), ans1quest3.getText().toString(), ans2quest3.getText().toString(), ans3quest3.getText().toString(), ans4quest3.getText().toString(), (spinner3.getSelectedItemPosition() + 1) + ""));
        questionsModels.add(new QuestionsModel(question4.getText().toString(), ans1quest4.getText().toString(), ans2quest4.getText().toString(), ans3quest4.getText().toString(), ans4quest4.getText().toString(), (spinner4.getSelectedItemPosition() + 1) + ""));
        questionsModels.add(new QuestionsModel(question5.getText().toString(), ans1quest5.getText().toString(), ans2quest5.getText().toString(), ans3quest5.getText().toString(), ans4quest5.getText().toString(), (spinner5.getSelectedItemPosition() + 1) + ""));

        testModel.nume = testName.getText().toString();
        testModel.questions = questionsModels;
        testModel.createdAt = System.currentTimeMillis() + "";
        testModel.createdBy = userModel.username;


        Map<String, Object> testModelMap = new HashMap<>();
        testModelMap.put("test_name", testModel.nume);
        testModelMap.put("created_at", testModel.createdAt);
        testModelMap.put("created_by", testModel.createdBy);
        Test test = new Test(testModel.nume, testModel.createdAt, testModel.createdBy, "", AppDatabase.getDatabase(CreateActivity.this).userDao().getAllUsers().get(0).id);
        int testId= (int)AppDatabase.getDatabase(CreateActivity.this).testCreatedDao().insert(test);
        List<Question> questions=new ArrayList<>();
        JSONArray questionsJSONArray = new JSONArray();
        for (QuestionsModel questionsModel : questionsModels) {
            JSONObject question = new JSONObject();
            try {
                questions.add(new Question(questionsModel.question,questionsModel.a1,questionsModel.a2,questionsModel.a3,questionsModel.a4,questionsModel.correctAnswer,testId));
                question.put("question", questionsModel.question);
                question.put("answer1", questionsModel.a1);
                question.put("answer2", questionsModel.a2);
                question.put("answer3", questionsModel.a3);
                question.put("answer4", questionsModel.a4);
                question.put("correct_answer", questionsModel.correctAnswer);
                questionsJSONArray.put(question);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        testModelMap.put("questions", questionsJSONArray.toString());

        for(Question question:questions){
            AppDatabase.getDatabase(CreateActivity.this).questionDao().insert(question);
        }

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("tests").add(testModelMap).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(CreateActivity.this, "SUCCESS", Toast.LENGTH_LONG).show();



                emptyAllFields();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(CreateActivity.this, "FAIL", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void emptyAllFields() {
        testName.setText("");
        question1.setText("");
        question2.setText("");
        question3.setText("");
        question4.setText("");
        question5.setText("");
        ans1quest1.setText("");
        ans2quest1.setText("");
        ans3quest1.setText("");
        ans4quest1.setText("");
        ans1quest2.setText("");
        ans2quest2.setText("");
        ans3quest2.setText("");
        ans4quest2.setText("");
        ans1quest3.setText("");
        ans2quest3.setText("");
        ans3quest3.setText("");
        ans4quest3.setText("");
        ans1quest4.setText("");
        ans2quest4.setText("");
        ans3quest4.setText("");
        ans4quest4.setText("");
        ans1quest5.setText("");
        ans2quest5.setText("");
        ans3quest5.setText("");
        ans4quest5.setText("");
    }

    private void setViews() {
        if (!userModel.type.equals(Constants.USER_TYPE_PROFESSOR)) {
            findViewById(R.id.teacher).setVisibility(View.VISIBLE);
        } else
            findViewById(R.id.teacher).setVisibility(View.GONE);
    }

}
