package com.example.alexandrusandu.proiectandroid.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.example.alexandrusandu.proiectandroid.R;
import com.example.alexandrusandu.proiectandroid.ui.common.BaseActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

public class ProfileActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_profile);
        super.onCreate(savedInstanceState);
        setViews();

    }


    private void setViews() {
        if(userModel.name.equals("")){
            ((TextView) findViewById(R.id.username)).setText(userModel.username);
        }else {
            ((TextView) findViewById(R.id.username)).setText(userModel.name);
        }
        ((TextView)findViewById(R.id.tests_taken)).setText("Tests taken: "+userModel.totalTests);
        ((TextView)findViewById(R.id.average)).setText("Average score: "+(Float.parseFloat(userModel.totalScore))/(Float.parseFloat(userModel.totalTests))+"/5");
    }

}
