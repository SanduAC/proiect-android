package com.example.alexandrusandu.proiectandroid.Models.UselessApiModels;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CityWeatherModel {
    private String id;
    private String name;
    private String lat;
    private String lon;
    private String country;
    private WeatherModel weatherModel;


    public CityWeatherModel(JSONObject object) {
        try {
            JSONObject city = object.optJSONObject("city");
            this.id=city.optString("id");
            this.name=city.optString("name");
            this.lat=city.optJSONObject("coord").optString("lat");
            this.lon=city.optJSONObject("coord").optString("lon");
            this.country=city.optString("country");

            JSONArray weather = object.optJSONArray("list");
            JSONObject main = weather.getJSONObject(0).optJSONObject("main");
            JSONObject weatherObject = weather.getJSONObject(0).optJSONArray("weather").getJSONObject(0);
            JSONObject wind = weather.getJSONObject(0).optJSONObject("wind");
            weatherModel = new WeatherModel(main.optString("temp"),
                    main.optString("temp_min"),
                    main.optString("temp_max"),
                    main.optString("pressure"),
                    main.optString("humidity"),
                    weatherObject.optString("main"),
                    wind.optString("speed"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    public String getCountry() {
        return country;
    }

    public WeatherModel getWeatherModel() {
        return weatherModel;
    }
}
