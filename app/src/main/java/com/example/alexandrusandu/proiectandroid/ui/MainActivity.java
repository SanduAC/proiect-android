package com.example.alexandrusandu.proiectandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.alexandrusandu.proiectandroid.Adapters.TestsRecyclerAdapter;
import com.example.alexandrusandu.proiectandroid.Database.AppDatabase;
import com.example.alexandrusandu.proiectandroid.Database.Models.User;
import com.example.alexandrusandu.proiectandroid.Utils.Constants;
import com.example.alexandrusandu.proiectandroid.ui.common.BaseActivity;
import com.example.alexandrusandu.proiectandroid.Models.TestModel;
import com.example.alexandrusandu.proiectandroid.Models.UselessApiModels.CityWeatherModel;
import com.example.alexandrusandu.proiectandroid.Models.UserModel;
import com.example.alexandrusandu.proiectandroid.R;
import com.example.alexandrusandu.proiectandroid.ServerIntegration.HttpService;
import com.example.alexandrusandu.proiectandroid.Utils.SharedPrefs;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends BaseActivity {
    List<TestModel> tests = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        getUserFromServer();
        setRecycler();

        //useless API call for FAZE 2.3. Utilizarea claselor pentru accesul la resurse externe (din rețea); (0.5 p.)
        getWeatherForCity();
    }

    private void setRecycler() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("tests")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            tests.add(new TestModel(document));
                        }
                        TestsRecyclerAdapter testsRecyclerAdapter = new TestsRecyclerAdapter(MainActivity.this, tests);
                        ((RecyclerView) findViewById(R.id.tests_recycler)).setAdapter(testsRecyclerAdapter);
                        ((RecyclerView) findViewById(R.id.tests_recycler))
                                .setLayoutManager(new LinearLayoutManager(MainActivity.this));
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(MainActivity.this, "Cannot load tests", Toast.LENGTH_LONG).show();
                    }
                });


    }

    private void getUserFromServer() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users").document(SharedPrefs.getUsernameFromPrefs(MainActivity.this.getApplicationContext()))
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        Map<String, Object> user = documentSnapshot.getData();
                        userModel = new UserModel(user);
                        AppDatabase.getDatabase(MainActivity.this).userDao().insert(new User(userModel));
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(MainActivity.this, "FAILED", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void getWeatherForCity() {
        String bucharestId = "683506";
        HttpService.getInstance().getWeatherInCity(MainActivity.this, bucharestId, new HttpService.RegularCallBack() {
            @Override
            public void onSuccess(CityWeatherModel cityWeatherModel) {
//                Toast.makeText(MainActivity.this, cityWeatherModel.toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFail(String error) {
//                Toast.makeText(MainActivity.this, error, Toast.LENGTH_LONG).show();
            }
        });
    }


    public void openTest(TestModel currentTest) {
        if(currentTest.createdBy.equals(userModel.username)){
            changeActivitWithoutClearingStack(MainActivity.this,ManageTestsActivity.class);
        }else{
            Intent intent = new Intent(MainActivity.this, TakeTestActivity.class);
            intent.putExtra(Constants.USER_ARGUMENT, userModel);
            intent.putExtra(Constants.TEST_ARGUMENT, currentTest);
//            intent.putExtra(Constants.QUESTIONS_ARGUMENT_1, currentTest.questions.get(0));
//            intent.putExtra(Constants.QUESTIONS_ARGUMENT_2, currentTest.questions.get(1));
//            intent.putExtra(Constants.QUESTIONS_ARGUMENT_3, currentTest.questions.get(2));
//            intent.putExtra(Constants.QUESTIONS_ARGUMENT_4, currentTest.questions.get(3));
//            intent.putExtra(Constants.QUESTIONS_ARGUMENT_5, currentTest.questions.get(4));
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserFromServer();
    }
}
