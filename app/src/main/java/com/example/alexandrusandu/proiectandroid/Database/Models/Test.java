package com.example.alexandrusandu.proiectandroid.Database.Models;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "tests")
public class Test {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    public Integer id;
    @NonNull
    @ColumnInfo(name = "nume")
    public String nume;
    @NonNull
    @ColumnInfo(name = "createdAt")
    public String createdAt;
    @NonNull
    @ColumnInfo(name = "createdBy")
    public String createdBy;
    @NonNull
    @ColumnInfo(name = "key")
    public String key;
    @NonNull
    @ColumnInfo(name = "createdById")
    public int createdById;

    public Test(@NonNull String nume, @NonNull String createdAt, @NonNull String createdBy, @NonNull String key, @NonNull int createdById) {
        this.nume = nume;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.key = key;
        this.createdById=createdById;
    }
}
