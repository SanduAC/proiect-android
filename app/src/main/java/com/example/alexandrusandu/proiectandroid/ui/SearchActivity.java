package com.example.alexandrusandu.proiectandroid.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.alexandrusandu.proiectandroid.Models.UselessApiModels.CityWeatherModel;
import com.example.alexandrusandu.proiectandroid.ServerIntegration.HttpService;
import com.example.alexandrusandu.proiectandroid.ui.common.BaseActivity;
import com.example.alexandrusandu.proiectandroid.R;

public class SearchActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_search);
        super.onCreate(savedInstanceState);
        Glide.with(SearchActivity.this)
                .load("https://picsum.photos/200/300")
                .into((ImageView) findViewById(R.id.photo));
    }
}
