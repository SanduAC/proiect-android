package com.example.alexandrusandu.proiectandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.alexandrusandu.proiectandroid.Models.UserModel;
import com.example.alexandrusandu.proiectandroid.R;
import com.example.alexandrusandu.proiectandroid.Utils.Constants;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class ProfileSettingsActivity extends AppCompatActivity {

    public UserModel userModel = new UserModel("", "", "", "", Constants.USER_TYPE_ADMIN, "");

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_settings);
        getUserModel();
        setViews();
        setControls();
    }

    private void setControls() {
        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }

        });

        findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                Map<String, Object> usermap = new HashMap<>();
                usermap.put("username", userModel.username);
                usermap.put("password", userModel.password);
                usermap.put("type", userModel.type);
                usermap.put("totalScore", userModel.totalScore);
                usermap.put("totalTests", userModel.totalTests);
                if (!RegisterActivity.isValidEmail(((EditText) findViewById(R.id.et_email)).getText().toString())) {
                    Toast.makeText(ProfileSettingsActivity.this, "Email invalid", Toast.LENGTH_LONG).show();
                    return;
                }
                usermap.put("email", ((EditText) findViewById(R.id.et_email)).getText().toString());
                usermap.put("gender", ((EditText) findViewById(R.id.et_gender)).getText().toString());
                usermap.put("name", ((EditText) findViewById(R.id.et_name)).getText().toString());
                db.collection("users").document(userModel.username).set(usermap).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(ProfileSettingsActivity.this,"Your data has been successfully modified!",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(ProfileSettingsActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.putExtra(Constants.USER_ARGUMENT, userModel);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }

                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ProfileSettingsActivity.this,"FAILED",Toast.LENGTH_LONG).show();
                    }
                });


            }

        });
    }

    private void setViews() {
        ((EditText) findViewById(R.id.et_name)).setText(userModel.name);
        ((EditText) findViewById(R.id.et_email)).setText(userModel.email);
        ((EditText) findViewById(R.id.et_gender)).setText(userModel.gender);
    }

    private void getUserModel() {
        if (getIntent().getSerializableExtra(Constants.USER_ARGUMENT) != null) {
            userModel = (UserModel) getIntent().getSerializableExtra(Constants.USER_ARGUMENT);
        }
    }


}
