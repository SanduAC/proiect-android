package com.example.alexandrusandu.proiectandroid.Database.Models;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.example.alexandrusandu.proiectandroid.Models.UserModel;

@Entity(tableName = "users")
public class User {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    public Integer id;
    @NonNull
    @ColumnInfo(name = "name")
    public String name;
    @NonNull
    @ColumnInfo(name = "username")
    public String username;
    @NonNull
    @ColumnInfo(name = "password")
    public String password;
    @NonNull
    @ColumnInfo(name = "email")
    public String email;
    @NonNull
    @ColumnInfo(name = "type")
    public String type;
    @NonNull
    @ColumnInfo(name = "gender")
    public String gender;
    @NonNull
    @ColumnInfo(name = "totalScore")
    public String totalScore;
    @NonNull
    @ColumnInfo(name = "totalTests")
    public String totalTests;


    public User(@NonNull String name, @NonNull String username, @NonNull String password, @NonNull String email, @NonNull String type, @NonNull String gender, @NonNull String totalScore, @NonNull String totalTests) {
        this.id=0;
        this.name = name;
        this.username = username;
        this.password = password;
        this.email = email;
        this.type = type;
        this.gender = gender;
        this.totalScore = totalScore;
        this.totalTests = totalTests;
    }
    public User(UserModel userModel){
        this.id=0;
        this.name = userModel.name;
        this.username = userModel.username;
        this.password = userModel.password;
        this.email = userModel.email;
        this.type = userModel.type;
        this.gender = userModel.gender;
        this.totalScore = userModel.totalScore;
        this.totalTests = userModel.totalTests;
    }
}
