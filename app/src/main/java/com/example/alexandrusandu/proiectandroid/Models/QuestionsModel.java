package com.example.alexandrusandu.proiectandroid.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class QuestionsModel implements Serializable {
    public String question;
    public String a1;
    public String a2;
    public String a3;
    public String a4;
    public String correctAnswer;

    public QuestionsModel(String question, String a1, String a2, String a3, String a4, String correctAnswer) {
        this.question = question;
        this.a1 = a1;
        this.a2 = a2;
        this.a3 = a3;
        this.a4 = a4;
        this.correctAnswer = correctAnswer;
    }
    public QuestionsModel(JSONObject question) {
        try {
            this.question = question.getString("question");
            this.a1 = question.getString("answer1");
            this.a2 = question.getString("answer2");
            this.a3 = question.getString("answer3");
            this.a4 = question.getString("answer4");
            this.correctAnswer = question.getString("correct_answer");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<QuestionsModel> fromJson(JSONArray questions) {
        ArrayList<QuestionsModel> questionsModels=new ArrayList<>();
        for(int i=0;i<questions.length();i++){
            try {
                questionsModels.add(new QuestionsModel(questions.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return questionsModels;
    }
}
