package com.example.alexandrusandu.proiectandroid.ui;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alexandrusandu.proiectandroid.Adapters.TestsRecyclerAdapter;
import com.example.alexandrusandu.proiectandroid.Database.AppDatabase;
import com.example.alexandrusandu.proiectandroid.Models.TestModel;
import com.example.alexandrusandu.proiectandroid.Models.UserModel;
import com.example.alexandrusandu.proiectandroid.R;
import com.example.alexandrusandu.proiectandroid.ui.common.BaseActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.LineChartView;
import lecho.lib.hellocharts.view.PieChartView;

public class RankingActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_ranking);
        super.onCreate(savedInstanceState);

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<UserModel> userModels = new ArrayList<>();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            Map<String, Object> user = document.getData();
                            userModel = new UserModel(user);
                            userModels.add(userModel);
                        }
                        String best1 = "";
                        String best2 = "";
                        String best3 = "";
                        UserModel bestUser1 = userModels.get(0);
                        if (bestUser1.name.equals("")) {
                            best1 = bestUser1.username;
                        } else {
                            best1 = bestUser1.name;
                        }
                        for (UserModel userModel : userModels) {
                            if (userModel.getAverage() > bestUser1.getAverage()) {
                                bestUser1 = userModel;
                                if (userModel.name.equals("")) {
                                    best1 = userModel.username;
                                } else {
                                    best1 = userModel.name;
                                }
                            }
                        }
                        userModels.remove(bestUser1);
                        UserModel bestUser2 = userModels.get(0);
                        if (bestUser2.name.equals("")) {
                            best2 = bestUser2.username;
                        } else {
                            best2 = bestUser2.name;
                        }
                        if (userModels.size() > 0) {
                            for (UserModel userModel : userModels) {
                                if (userModel.getAverage() > bestUser2.getAverage()) {
                                    bestUser2 = userModel;
                                    if (bestUser2.name.equals("")) {
                                        best2 = bestUser2.username;
                                    } else {
                                        best2 = bestUser2.name;
                                    }
                                    userModels.remove(userModel);
                                }
                            }
                            userModels.remove(bestUser2);
                        }
                        UserModel bestUser3 = userModels.get(0);
                        if (bestUser3.name.equals("")) {
                            best3 = bestUser3.username;
                        } else {
                            best3 = bestUser3.name;
                        }
                        if (userModels.size() > 0) {
                            for (UserModel userModel : userModels) {
                                if (userModel.getAverage() > bestUser3.getAverage()) {
                                    bestUser3 = userModel;
                                    if (bestUser3.name.equals("")) {
                                        best3 = bestUser3.username;
                                    } else {
                                        best3 = bestUser3.name;
                                    }
                                }
                            }
                            userModels.remove(bestUser3);
                        }
                        ((TextView) findViewById(R.id.best1)).setText("1. " + best1 + " " + bestUser1.getAverage());
                        ((TextView) findViewById(R.id.best2)).setText("2. " + best2 + " " + bestUser2.getAverage());
                        ((TextView) findViewById(R.id.best3)).setText("3. " + best3 + " " + bestUser3.getAverage());


                        generateData(bestUser1, bestUser2, bestUser3);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(RankingActivity.this, "Cannot load tests", Toast.LENGTH_LONG).show();
                    }
                });
    }


    private void generateData(UserModel best1, UserModel best2, UserModel best3) {

        List<SliceValue> values = new ArrayList<SliceValue>();

        SliceValue sliceValue1 = new SliceValue((float) best1.getAverage(), ChartUtils.pickColor());
        values.add(sliceValue1);
        SliceValue sliceValue2 = new SliceValue((float) best2.getAverage(), ChartUtils.pickColor());
        values.add(sliceValue2);
        SliceValue sliceValue3 = new SliceValue((float) best3.getAverage(), ChartUtils.pickColor());
        values.add(sliceValue3);

        PieChartData data = new PieChartData(values);
        data.setHasLabels(false);
        data.setHasLabelsOutside(true);
        data.setHasCenterCircle(false);


        data.setCenterText1("Hello!");

        data.setCenterText2("Charts (Roboto Italic)");

        ((PieChartView) findViewById(R.id.chart)).setPieChartData(data);
    }


}
