package com.example.alexandrusandu.proiectandroid.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.alexandrusandu.proiectandroid.Database.AppDatabase;
import com.example.alexandrusandu.proiectandroid.Database.Models.Test;
import com.example.alexandrusandu.proiectandroid.Database.Models.TestTaken;
import com.example.alexandrusandu.proiectandroid.Database.Models.User;
import com.example.alexandrusandu.proiectandroid.Models.UserModel;
import com.example.alexandrusandu.proiectandroid.R;
import com.example.alexandrusandu.proiectandroid.Utils.Constants;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends AppCompatActivity {

    public UserModel userModel = new UserModel("","","","", Constants.USER_TYPE_ADMIN, "");


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getUserModel();
        setViews();
        setControls();
    }

    private void setViews() {
        ((TextView)findViewById(R.id.tv_username)).setText(userModel.username);
    }

    private void getUserModel() {
        if (getIntent().getSerializableExtra(Constants.USER_ARGUMENT) != null) {
            userModel = (UserModel) getIntent().getSerializableExtra(Constants.USER_ARGUMENT);
        }
    }

    private void setControls() {
        findViewById(R.id.btn_profile_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, ProfileSettingsActivity.class);
                intent.putExtra(Constants.USER_ARGUMENT, userModel);
                startActivity(intent);
            }
        });
        findViewById(R.id.btn_manage_tests).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsActivity.this, ManageTestsActivity.class));
            }
        });
        findViewById(R.id.btn_save_file).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveFile();
            }
        });
        findViewById(R.id.btn_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(SettingsActivity.this, LoginActivity.class);
                loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginIntent);

            }
        });
        findViewById(R.id.btn_get_university_loc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(SettingsActivity.this, UnviersityLocationActivity.class);
                startActivity(loginIntent);
            }
        });
    }

    private void saveFile() {
        User user=AppDatabase.getDatabase(SettingsActivity.this).userDao().getAllUsers().get(0);
        if(user.type.equals("0")){
            saveFileForTeacher(user);
        }else{
            saveFileForStudent(user);
        }
    }

    private void saveFileForStudent(User user) {
        List<TestTaken> tests=new ArrayList<>(AppDatabase.getDatabase(SettingsActivity.this).testDao().getAllTestsForUser(user.id));

        String filename = "TestsTaken.txt";
        String fileContents = "";

        for(TestTaken test : tests){
            fileContents=fileContents+"\n"+test.toString();
        }


        File directory = getFilesDir();
        File file = new File(directory, filename);
        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(file);
            try {
                stream.write(fileContents.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void saveFileForTeacher(User user){
        List<Test> tests=new ArrayList<>(AppDatabase.getDatabase(SettingsActivity.this)
                .testCreatedDao().getAllTestsCreatedBy(user.id));

        String filename = "TestsCreated.txt";
        String fileContents = "";

        for(Test test : tests){
            fileContents=fileContents+"\n"+test.toString();
        }


        File directory = getFilesDir();
        File file = new File(directory, filename);
        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(file);
            try {
                stream.write(fileContents.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
