package com.example.alexandrusandu.proiectandroid.Database.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "questions")
public class Question {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    public Integer id;

    @NonNull
    @ColumnInfo(name = "question")
    public String question;
    @NonNull
    @ColumnInfo(name = "a1")
    public String a1;
    @NonNull
    @ColumnInfo(name = "a2")
    public String a2;
    @NonNull
    @ColumnInfo(name = "a3")
    public String a3;
    @NonNull
    @ColumnInfo(name = "a4")
    public String a4;
    @NonNull
    @ColumnInfo(name = "correctAnswer")
    public String correctAnswer;
    @NonNull
    @ColumnInfo(name = "testId")
    public int testId;

    public Question(@NonNull String question, @NonNull String a1, @NonNull String a2, @NonNull String a3, @NonNull String a4, @NonNull String correctAnswer, @NonNull int testId) {
        this.question = question;
        this.a1 = a1;
        this.a2 = a2;
        this.a3 = a3;
        this.a4 = a4;
        this.correctAnswer = correctAnswer;
        this.testId = testId;
    }
}
