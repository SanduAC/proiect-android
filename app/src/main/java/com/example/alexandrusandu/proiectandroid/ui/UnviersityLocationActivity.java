package com.example.alexandrusandu.proiectandroid.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.example.alexandrusandu.proiectandroid.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class UnviersityLocationActivity extends FragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_university);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                LatLng latLng =
                        new LatLng(44.447589, 26.096992);
                googleMap.addMarker(new MarkerOptions().position(latLng)
                        .title("ASE WOW"));
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));

                //44.447589, 26.096992
            }
        });
        setMap();
    }

    private void setMap() {

    }
}
