package com.example.alexandrusandu.proiectandroid.Database.Models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "tests_taken")
public class TestTaken {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private Integer id;

    @NonNull
    @ColumnInfo(name = "test_name")
    private String test_name;

    @NonNull
    @ColumnInfo(name = "question1")
    private String question1;

    @NonNull
    @ColumnInfo(name = "correct1")
    private boolean correct1;

    @NonNull
    @ColumnInfo(name = "question2")
    private String question2;

    @NonNull
    @ColumnInfo(name = "correct2")
    private boolean correct2;

    @NonNull
    @ColumnInfo(name = "question3")
    private String question3;

    @NonNull
    @ColumnInfo(name = "correct3")
    private boolean correct3;

    @NonNull
    @ColumnInfo(name = "question4")
    private String question4;

    @NonNull
    @ColumnInfo(name = "correct4")
    private boolean correct4;

    @NonNull
    @ColumnInfo(name = "question5")
    private String question5;

    @NonNull
    @ColumnInfo(name = "correct5")
    private boolean correct5;

    @NonNull
    @ColumnInfo(name = "student")
    public int student;

    public TestTaken(@NonNull String test_name, @NonNull String question1, @NonNull boolean correct1, @NonNull String question2, @NonNull boolean correct2, @NonNull String question3, @NonNull boolean correct3, @NonNull String question4, @NonNull boolean correct4, @NonNull String question5, @NonNull boolean correct5, @NonNull int student) {
        this.test_name = test_name;
        this.question1 = question1;
        this.correct1 = correct1;
        this.question2 = question2;
        this.correct2 = correct2;
        this.question3 = question3;
        this.correct3 = correct3;
        this.question4 = question4;
        this.correct4 = correct4;
        this.question5 = question5;
        this.correct5 = correct5;
        this.student=student;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    public void setTest_name(@NonNull String test_name) {
        this.test_name = test_name;
    }

    public void setQuestion1(@NonNull String question1) {
        this.question1 = question1;
    }

    public void setCorrect1(@NonNull boolean correct1) {
        this.correct1 = correct1;
    }

    public void setQuestion2(@NonNull String question2) {
        this.question2 = question2;
    }


    public void setCorrect2(@NonNull boolean correct2) {
        this.correct2 = correct2;
    }

    public void setQuestion3(@NonNull String question3) {
        this.question3 = question3;
    }


    public void setCorrect3(@NonNull boolean correct3) {
        this.correct3 = correct3;
    }

    public void setQuestion4(@NonNull String question4) {
        this.question4 = question4;
    }


    public void setCorrect4(@NonNull boolean correct4) {
        this.correct4 = correct4;
    }

    public void setQuestion5(@NonNull String question5) {
        this.question5 = question5;
    }


    public void setCorrect5(@NonNull boolean correct5) {
        this.correct5 = correct5;
    }

    @NonNull
    public Integer getId() {
        return id;
    }

    @NonNull
    public String getTest_name() {
        return test_name;
    }

    @NonNull
    public String getQuestion1() {
        return question1;
    }


    @NonNull
    public boolean isCorrect1() {
        return correct1;
    }

    @NonNull
    public String getQuestion2() {
        return question2;
    }


    @NonNull
    public boolean isCorrect2() {
        return correct2;
    }

    @NonNull
    public String getQuestion3() {
        return question3;
    }


    @NonNull
    public boolean isCorrect3() {
        return correct3;
    }

    @NonNull
    public String getQuestion4() {
        return question4;
    }


    @NonNull
    public boolean isCorrect4() {
        return correct4;
    }

    @NonNull
    public String getQuestion5() {
        return question5;
    }


    @NonNull
    public boolean isCorrect5() {
        return correct5;
    }
}