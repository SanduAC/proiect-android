package com.example.alexandrusandu.proiectandroid.Database;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.alexandrusandu.proiectandroid.Database.Daos.QuestionDao;
import com.example.alexandrusandu.proiectandroid.Database.Daos.TestCreatedDao;
import com.example.alexandrusandu.proiectandroid.Database.Daos.TestDao;
import com.example.alexandrusandu.proiectandroid.Database.Daos.UserDao;
import com.example.alexandrusandu.proiectandroid.Database.Models.Question;
import com.example.alexandrusandu.proiectandroid.Database.Models.Test;
import com.example.alexandrusandu.proiectandroid.Database.Models.TestTaken;
import com.example.alexandrusandu.proiectandroid.Database.Models.User;

@Database(entities = {TestTaken.class, Question.class, Test.class, User.class}, version = 3, exportSchema = true)
public abstract class AppDatabase extends RoomDatabase {

    public abstract TestDao testDao();
    public abstract UserDao userDao();
    public abstract QuestionDao questionDao();
    public abstract TestCreatedDao testCreatedDao();

    private static volatile AppDatabase INSTANCE;

    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class,
                            "app_database")
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}