package com.example.alexandrusandu.proiectandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.alexandrusandu.proiectandroid.R;
import com.example.alexandrusandu.proiectandroid.Utils.Constants;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setControls();
    }

    private void setControls() {
        findViewById(R.id.btn_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendToDatabase();
            }
        });
    }


    private void sendToDatabase() {
        if (((EditText) findViewById(R.id.et_username)).getText().toString().trim().equals("")) {
            Toast.makeText(RegisterActivity.this, "Username can not be empty!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (((EditText) findViewById(R.id.et_email)).getText().toString().trim().equals("")) {
            Toast.makeText(RegisterActivity.this, "Email can not be empty!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!isValidEmail(((EditText) findViewById(R.id.et_email)).getText().toString())) {
            Toast.makeText(RegisterActivity.this, "Email is not valid!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (((EditText) findViewById(R.id.et_password)).getText().toString().trim().equals("")) {
            Toast.makeText(RegisterActivity.this, "Password can not be empty!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!((EditText) findViewById(R.id.et_password)).getText().toString().equals(((EditText) findViewById(R.id.et_password_verification)).getText().toString())) {
            Toast.makeText(RegisterActivity.this, "Passwords do not match!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!((RadioButton)findViewById(R.id.student_radio)).isChecked()&&!((RadioButton)findViewById(R.id.professor_radio)).isChecked()){
            Toast.makeText(RegisterActivity.this, "Please select your account type!", Toast.LENGTH_SHORT).show();
            return;
        }

        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users").document(((EditText) findViewById(R.id.et_username)).getText().toString())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.getData() != null){
                            Toast.makeText(RegisterActivity.this, "This username is already taken.",
                                    Toast.LENGTH_LONG).show();
                        }else{
                            Map<String, Object> user = new HashMap<>();
                            user.put("username", ((EditText) findViewById(R.id.et_username))
                                    .getText().toString());
                            user.put("email", ((EditText) findViewById(R.id.et_email)).getText().toString());
                            user.put("password", ((EditText) findViewById(R.id.et_password)).getText().toString());
                            if(((RadioButton)findViewById(R.id.student_radio)).isChecked()){
                                user.put("type", Constants.USER_TYPE_STUDENT);
                            }else if(((RadioButton)findViewById(R.id.professor_radio)).isChecked()){
                                user.put("type", Constants.USER_TYPE_PROFESSOR);
                            }
                            db.collection("users")
                                    .document(((EditText) findViewById(R.id.et_username))
                                            .getText()
                                            .toString())
                                    .set(user)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(RegisterActivity.this, "Your account has been successfully created.", Toast.LENGTH_LONG).show();
                                            startActivity(new Intent(RegisterActivity.this,
                                                    LoginActivity.class));
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(RegisterActivity.this, "FAIL",
                                                    Toast.LENGTH_LONG).show();
                                        }
                                    });
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(RegisterActivity.this, "FAIL", Toast.LENGTH_LONG).show();
                    }
                });

    }
    public static boolean isValidEmail(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
}
