package com.example.alexandrusandu.proiectandroid.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.alexandrusandu.proiectandroid.R;
import com.example.alexandrusandu.proiectandroid.ui.common.BaseActivity;

public class TestsRakingActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_tests_ranking);
        super.onCreate(savedInstanceState);
    }
}
