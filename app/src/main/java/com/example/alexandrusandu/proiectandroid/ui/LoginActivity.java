package com.example.alexandrusandu.proiectandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.alexandrusandu.proiectandroid.R;
import com.example.alexandrusandu.proiectandroid.Utils.SharedPrefs;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;



public class LoginActivity extends AppCompatActivity {

    private boolean shouldExit = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setControls();
    }


    public void  showLoading(Boolean show) {
        if (show) {
            findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.progressBar).setVisibility(View.GONE);
        }
    }

    private void setControls() {
        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading(true);
                final EditText etUsername = findViewById(R.id.et_username);
                final EditText etPassword = findViewById(R.id.et_password);
                if (etUsername.getText().toString().equals("") || etPassword.getText().toString().equals("")) {
                    Toast.makeText(LoginActivity.this, "All the fields are mandatory", Toast.LENGTH_LONG).show();
                    return;
                }

                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("users").document(etUsername.getText().toString())
                        .get()
                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                Map<String, Object> user = documentSnapshot.getData();
                                if(user==null){
                                    Toast.makeText(LoginActivity.this, "The account does not exist.", Toast.LENGTH_LONG).show();
                                    showLoading(false);
                                    return;
                                }
                                if(user.get("password").equals(etPassword.getText().toString())){
                                    Intent mainActivityIntent = new Intent(LoginActivity.this, MainActivity.class);
                                    SharedPrefs.seveUsernameInPrefs(etUsername.getText().toString(), LoginActivity.this.getApplicationContext());
                                    Toast.makeText(LoginActivity.this, "You have logged in successfully.", Toast.LENGTH_LONG).show();
                                    showLoading(false);
                                    startActivity(mainActivityIntent);
                                }else{
                                    Toast.makeText(LoginActivity.this, "The password is incorrect.", Toast.LENGTH_LONG).show();
                                    showLoading(false);
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(LoginActivity.this, "FAILED", Toast.LENGTH_LONG).show();
                                showLoading(false);
                            }
                        });


            }
        });
        findViewById(R.id.tv_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainActivityIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(mainActivityIntent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (shouldExit) {
            super.onBackPressed();
        }
        if (!shouldExit) {
            Toast.makeText(this, "Press again in maximul 3 seconds to exit", Toast.LENGTH_SHORT).show();
            shouldExit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    shouldExit = false;
                }
            }, 3000);
        }
    }
}
