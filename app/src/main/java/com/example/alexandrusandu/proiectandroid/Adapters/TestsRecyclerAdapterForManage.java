package com.example.alexandrusandu.proiectandroid.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alexandrusandu.proiectandroid.Database.Models.Test;
import com.example.alexandrusandu.proiectandroid.Models.TestModel;
import com.example.alexandrusandu.proiectandroid.R;
import com.example.alexandrusandu.proiectandroid.ui.MainActivity;
import com.example.alexandrusandu.proiectandroid.ui.ManageTestsActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TestsRecyclerAdapterForManage extends RecyclerView.Adapter<TestsRecyclerAdapterForManage.ViewHolder> {

    List<Test> tests;
    ManageTestsActivity manageTestsActivity;

    public TestsRecyclerAdapterForManage(ManageTestsActivity manageTestsActivity, List<Test> tests) {
        this.tests = tests;
        this.manageTestsActivity = manageTestsActivity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.test_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        final TestsRecyclerAdapterForManage.ViewHolder vh = new TestsRecyclerAdapterForManage.ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        if (position == tests.size()-1) {
            viewHolder.endDivider.setVisibility(View.VISIBLE);
        }

        final Test currentTest = tests.get(position);
        viewHolder.name.setText(currentTest.nume);
        viewHolder.createdBy.setText(currentTest.createdBy);

        Date date = new Date(Long.valueOf(currentTest.createdAt));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT+3"));
        String formattedDate = sdf.format(date);

        viewHolder.createdAt.setText(formattedDate);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageTestsActivity.deleteTest(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView createdBy;
        TextView createdAt;
        View endDivider;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.nume_test);
            createdBy = itemView.findViewById(R.id.created_by);
            createdAt = itemView.findViewById(R.id.created_at);
            endDivider = itemView.findViewById(R.id.end_divider);
        }
    }
}
