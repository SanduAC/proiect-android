package com.example.alexandrusandu.proiectandroid.ServerIntegration;

import android.content.Context;

import com.example.alexandrusandu.proiectandroid.Models.UselessApiModels.CityWeatherModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class HttpService {

    private static HttpService mInstance;
    private Retrofit retrofit;

    public static HttpService getInstance() {
        if (mInstance == null)
            mInstance = new HttpService();
        return mInstance;
    }

    public static HttpService rebuildInstance() {
        mInstance = new HttpService();
        return mInstance;
    }

    private HttpService() {
        //block
    }

    public interface HttpInterface {
        @GET("forecast?APPID=b498f9b1a5b7e65ee316c8a8f8f16a44")
        Call<ResponseBody> getWeatherInCity(@Query("id") String taskId);

        @GET("")
        Call<ResponseBody> getPhoto();
    }

    private HttpInterface getHttpInterface(final Context context) {
        if (retrofit == null) {
            HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
            logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(logInterceptor)
                    .cookieJar(new CookieJar() {
                        @Override
                        public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
                            CookieStore.getInstance(context).save(cookies, url);
                        }

                        @Override
                        public List<Cookie> loadForRequest(HttpUrl url) {
                            List<Cookie> cookies =CookieStore.getInstance(context).getCookies(url);
                            return cookies != null ? cookies : new ArrayList<Cookie>();
                        }
                    })
                    .writeTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES)
                    .build();
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://api.openweathermap.org/data/2.5/")
                    .client(client)
                    .build();
        }
        return retrofit.create(HttpInterface.class);
    }
    private HttpInterface getHttpInterface(final Context context, String baseURL) {
        if (retrofit == null) {
            HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
            logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(logInterceptor)
                    .cookieJar(new CookieJar() {
                        @Override
                        public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
                            CookieStore.getInstance(context).save(cookies, url);
                        }

                        @Override
                        public List<Cookie> loadForRequest(HttpUrl url) {
                            List<Cookie> cookies =CookieStore.getInstance(context).getCookies(url);
                            return cookies != null ? cookies : new ArrayList<Cookie>();
                        }
                    })
                    .writeTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES)
                    .build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseURL)
                    .client(client)
                    .build();
        }
        return retrofit.create(HttpInterface.class);
    }

    //CALL-BACKS
    public interface RegularCallBack {
        void onSuccess(CityWeatherModel cityWeatherModel);
        void onFail(String error);
    }
    //CALL-BACKS
    public interface PhotoCallBack {
        void onSuccess(CityWeatherModel cityWeatherModel);
        void onFail(String error);
    }

    //CALLS
    public void getWeatherInCity(Context context, String cityId, final RegularCallBack regularCallBack) {
        final Call<ResponseBody> call = getHttpInterface(context).getWeatherInCity(cityId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String raw = response.body().string();
                    JSONObject jsonRaw = new JSONObject(raw);
                    if (jsonRaw.optInt("cod")!=200) {
                        regularCallBack.onFail(jsonRaw.optString("message"));
                        return;
                    }
                    regularCallBack.onSuccess(new CityWeatherModel(jsonRaw));
                } catch (Exception e) {
                    regularCallBack.onFail(e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                regularCallBack.onFail(t.getLocalizedMessage());
            }
        });
    }

    public void getPhoto(Context context, String baseURL, final PhotoCallBack photoCallBack) {
        final Call<ResponseBody> call = getHttpInterface(context, baseURL).getPhoto();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String raw = response.body().string();
                    JSONObject jsonRaw = new JSONObject(raw);
                    if (jsonRaw.optInt("cod")!=200) {
                        photoCallBack.onFail(jsonRaw.optString("message"));
                        return;
                    }
                    photoCallBack.onSuccess(new CityWeatherModel(jsonRaw));
                } catch (Exception e) {
                    photoCallBack.onFail(e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                photoCallBack.onFail(t.getLocalizedMessage());
            }
        });
    }

}
