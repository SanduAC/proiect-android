package com.example.alexandrusandu.proiectandroid.ui.common;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alexandrusandu.proiectandroid.Models.UserModel;
import com.example.alexandrusandu.proiectandroid.R;
import com.example.alexandrusandu.proiectandroid.Utils.Constants;
import com.example.alexandrusandu.proiectandroid.ui.CreateActivity;
import com.example.alexandrusandu.proiectandroid.ui.MainActivity;
import com.example.alexandrusandu.proiectandroid.ui.ProfileActivity;
import com.example.alexandrusandu.proiectandroid.ui.RankingActivity;
import com.example.alexandrusandu.proiectandroid.ui.SearchActivity;
import com.example.alexandrusandu.proiectandroid.ui.SettingsActivity;
import com.example.alexandrusandu.proiectandroid.ui.TestsRakingActivity;

import java.util.List;

public class BaseActivity extends AppCompatActivity {

    private boolean shouldExit=false;
    public UserModel userModel = new UserModel("","","","", Constants.USER_TYPE_ADMIN, "");

    private List<Dialog> dialogs;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setButtonViews();
        getUserModel();
        setActionBarView();
        setButtonsControls();
    }

    private void getUserModel() {
        if(getIntent().getSerializableExtra(Constants.USER_ARGUMENT)!=null){
            userModel= (UserModel) getIntent().getSerializableExtra(Constants.USER_ARGUMENT);
        }
    }

    private void setActionBarView() {
        if (this instanceof MainActivity){
            ((TextView) findViewById(R.id.option1)).setText("Feed");
            findViewById(R.id.divider).setVisibility(View.GONE);
            findViewById(R.id.option2).setVisibility(View.GONE);
        }
        if (this instanceof CreateActivity){
            ((TextView) findViewById(R.id.option1)).setText("Create");
            findViewById(R.id.divider).setVisibility(View.GONE);
            findViewById(R.id.option2).setVisibility(View.GONE);
        }
        if (this instanceof ProfileActivity){
            ((TextView) findViewById(R.id.option1)).setText("Profile");
            findViewById(R.id.divider).setVisibility(View.GONE);
            findViewById(R.id.option2).setVisibility(View.GONE);
            findViewById(R.id.btn_settings).setVisibility(View.VISIBLE);
        }
        if (this instanceof SearchActivity){
            ((TextView) findViewById(R.id.option1)).setText("Search");
            findViewById(R.id.divider).setVisibility(View.GONE);
            findViewById(R.id.option2).setVisibility(View.GONE);
        }
        if (this instanceof RankingActivity){
            ((TextView) findViewById(R.id.option1)).setText("Users");
            ((TextView) findViewById(R.id.option2)).setText("Tests");
            findViewById(R.id.option2).setAlpha(0.5f);
        }
        if (this instanceof TestsRakingActivity){
            ((TextView) findViewById(R.id.option1)).setText("Users");
            ((TextView) findViewById(R.id.option2)).setText("Tests");
            findViewById(R.id.option1).setAlpha(0.5f);
        }
    }

    public void setButtonViews() {
        ((ImageView) findViewById(R.id.news)).setImageDrawable(getResources().getDrawable(R.drawable.btn_menu_feed_inactive_blue));
        ((ImageView) findViewById(R.id.profile)).setImageDrawable(getResources().getDrawable(R.drawable.btn_menu_profile_inactive));
        ((ImageView) findViewById(R.id.create)).setImageDrawable(getResources().getDrawable(R.drawable.btn_create_inactive));
        ((ImageView) findViewById(R.id.search)).setImageDrawable(getResources().getDrawable(R.drawable.btn_search_inactive));
        ((ImageView) findViewById(R.id.ranking)).setImageDrawable(getResources().getDrawable(R.drawable.btn_ranking_inactive));

        if (this instanceof MainActivity) {
            ((ImageView) findViewById(R.id.news)).setImageDrawable(getResources().getDrawable(R.drawable.btn_menu_feed_active_blue));
        }
        if(this instanceof ProfileActivity){
            ((ImageView) findViewById(R.id.profile)).setImageDrawable(getResources().getDrawable(R.drawable.btn_menu_profile_active));
        }
        if(this instanceof CreateActivity){
            ((ImageView) findViewById(R.id.create)).setImageDrawable(getResources().getDrawable(R.drawable.btn_create_active));
        }
        if(this instanceof SearchActivity){
            ((ImageView) findViewById(R.id.search)).setImageDrawable(getResources().getDrawable(R.drawable.btn_search_active));
        }
        if(this instanceof RankingActivity){
            ((ImageView) findViewById(R.id.ranking)).setImageDrawable(getResources().getDrawable(R.drawable.btn_ranking_active));
        }
    }

    public void setButtonsControls() {
        findViewById(R.id.news).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BaseActivity.this instanceof MainActivity){
                    return;
                }
                changeActivityAndClearStack(BaseActivity.this, MainActivity.class);
            }
        });
        findViewById(R.id.profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BaseActivity.this instanceof ProfileActivity){
                    return;
                }
                changeActivityAndClearStack(BaseActivity.this, ProfileActivity.class);
            }
        });
        findViewById(R.id.create).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BaseActivity.this instanceof CreateActivity){
                    return;
                }
                changeActivityAndClearStack(BaseActivity.this, CreateActivity.class);
            }
        });
        findViewById(R.id.search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BaseActivity.this instanceof SearchActivity){
                    return;
                }
                changeActivityAndClearStack(BaseActivity.this, SearchActivity.class);
            }
        });
        findViewById(R.id.ranking).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BaseActivity.this instanceof RankingActivity){
                    return;
                }
                changeActivityAndClearStack(BaseActivity.this, RankingActivity.class);
            }
        });
        if(this instanceof RankingActivity||this instanceof TestsRakingActivity) {
            findViewById(R.id.option1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    findViewById(R.id.option2).setAlpha(0.5f);
                    findViewById(R.id.option1).setAlpha(1f);
                    if(BaseActivity.this instanceof RankingActivity){
                        return;
                    }
                    changeActivityAndClearStack(BaseActivity.this, RankingActivity.class);
                }
            });
            findViewById(R.id.option2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    findViewById(R.id.option1).setAlpha(0.5f);
                    findViewById(R.id.option2).setAlpha(1f);
                    if(BaseActivity.this instanceof TestsRakingActivity){
                        return;
                    }
                    changeActivityAndClearStack(BaseActivity.this, TestsRakingActivity.class);
                }
            });
        }
        findViewById(R.id.btn_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BaseActivity.this, SettingsActivity.class);
                intent.putExtra(Constants.USER_ARGUMENT, userModel);
                startActivity(intent);
            }
        });
    }

    private void changeActivityAndClearStack(BaseActivity baseActivity, Class baseActivityClass){
        Intent intent = new Intent(baseActivity, baseActivityClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(Constants.USER_ARGUMENT, userModel);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void changeActivitWithoutClearingStack(BaseActivity baseActivity, Class baseActivityClass){
        Intent intent = new Intent(baseActivity, baseActivityClass);
        intent.putExtra(Constants.USER_ARGUMENT, userModel);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }


    public void  showLoading(Boolean show) {
        if (show) {
            findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.progressBar).setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if (shouldExit) {
            super.onBackPressed();
        }
        if (!shouldExit) {
            Toast.makeText(this, "Press again in maximum 2 seconds to exit", Toast.LENGTH_SHORT).show();
            shouldExit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    shouldExit = false;
                }
            }, 3000);
        }
    }



    public interface OnDialogButtonsPressed {
        void onPositiveButtonPressed();

        void onNegativeButtonPressed();
    }


    public void showDialogMessage(Boolean cancelable, String message, String optionPositive, String optionNegative, final OnDialogButtonsPressed onDialogButtonsPressed) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        if (onDialogButtonsPressed != null)
                            onDialogButtonsPressed.onPositiveButtonPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        if (onDialogButtonsPressed != null)
                            onDialogButtonsPressed.onNegativeButtonPressed();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog dialog;
        builder.setCancelable(cancelable);
        if (optionNegative == null)
            builder.setMessage(message).setPositiveButton(optionPositive, dialogClickListener);

        if (optionPositive == null)
            builder.setMessage(message).setNegativeButton(optionNegative, dialogClickListener);

        if (optionNegative != null && optionPositive != null)
            builder.setMessage(message).setNegativeButton(optionNegative, dialogClickListener)
                    .setPositiveButton(optionPositive, dialogClickListener);

        if (!this.isFinishing()) {
            dialog = builder.show();
            dialogs.add(dialog);
        }

    }

}
