package com.example.alexandrusandu.proiectandroid.Models;


import android.util.Log;

import com.example.alexandrusandu.proiectandroid.Models.QuestionsModel;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class TestModel implements Serializable {
    public ArrayList<QuestionsModel> questions;
    public String nume;
    public String createdAt;
    public String createdBy;
    public String key;

    public TestModel(ArrayList<QuestionsModel> questions, String nume, String createdAt, String createdBy, String key) {
        this.questions = questions;
        this.nume = nume;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.key = key;
    }

    public TestModel(QueryDocumentSnapshot document) {
        nume = document.get("test_name").toString();
        createdAt = document.get("created_at").toString();
        createdBy = document.get("created_by").toString();
        try {
            questions=QuestionsModel.fromJson(new JSONArray(document.get("questions").toString()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        key = "";
    }

}
