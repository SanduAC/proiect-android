package com.example.alexandrusandu.proiectandroid.Database.Daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.alexandrusandu.proiectandroid.Database.Models.TestTaken;

import java.util.List;

@Dao
public interface TestDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TestTaken test);

    @Query("DELETE FROM tests_taken")
    void deleteAll();

    @Query("DELETE FROM tests_taken WHERE id=:testId")
    void deleteTest(int testId);


    @Query("SELECT * from tests_taken")
    List<TestTaken> getAllTests();

    @Query("SELECT * from tests_taken WHERE student=:userId")
    List<TestTaken> getAllTestsForUser(int userId);

    @Query("SELECT * from tests_taken WHERE student=:student")
    List<TestTaken> getAllTests(int student);
}