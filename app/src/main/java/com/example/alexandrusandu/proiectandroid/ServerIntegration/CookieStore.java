package com.example.alexandrusandu.proiectandroid.ServerIntegration;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import okhttp3.Cookie;
import okhttp3.HttpUrl;

public class CookieStore {

    private static CookieStore mCookieStore;
    private Context context;

    private CookieStore(Context c) {
        this.context = c;
    }

    private CookieStore() {
        //block
    }

    public static CookieStore getInstance(Context c) {
        if (mCookieStore == null)
            return new CookieStore(c);
        if (mCookieStore.context == null) {
            mCookieStore = null;
            mCookieStore = new CookieStore(c);
        }
        return mCookieStore;
    }

    private SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences("COOKIE_PREFS", Context.MODE_PRIVATE);
    }

    public void save(List<Cookie> cookies, HttpUrl url) {
        String[] cookiesAsString = new String[cookies.size()];
        for (int i = 0; i < cookies.size(); i++) {
            cookiesAsString[i] = cookies.get(i).toString();
        }
        getSharedPreferences().edit().putStringSet("saved_cookies", new HashSet<>(Arrays.asList(cookiesAsString))).commit();
    }

    public List<Cookie> getCookies(HttpUrl url) {
        Set<String> cookieSet = getSharedPreferences().getStringSet("saved_cookies", new HashSet<String>());
        List<Cookie> cookies = new ArrayList<>();
        for (String cookieAsString : cookieSet) {
            cookies.add(Cookie.parse(url,cookieAsString));
        }
        return new ArrayList<>(cookies);
    }


}
