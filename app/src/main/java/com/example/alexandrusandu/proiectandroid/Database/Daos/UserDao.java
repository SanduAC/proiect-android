package com.example.alexandrusandu.proiectandroid.Database.Daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.alexandrusandu.proiectandroid.Database.Models.TestTaken;
import com.example.alexandrusandu.proiectandroid.Database.Models.User;

import java.util.List;

@Dao
public interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(User user);

    @Query("DELETE FROM users")
    void deleteAll();

    @Query("SELECT * from users")
    List<User> getAllUsers();
}