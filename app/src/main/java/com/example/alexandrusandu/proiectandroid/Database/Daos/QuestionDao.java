package com.example.alexandrusandu.proiectandroid.Database.Daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.alexandrusandu.proiectandroid.Database.Models.Question;
import com.example.alexandrusandu.proiectandroid.Database.Models.TestTaken;

import java.util.List;

@Dao
public interface QuestionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Question question);

    @Query("DELETE FROM questions")
    void deleteAll();

    @Query("DELETE FROM questions WHERE id=:testId")
    void deleteForTest(int testId);

    @Query("SELECT * from questions")
    List<Question> getAllQuestions();

    @Query("SELECT * from questions WHERE testId=:testId")
    List<Question> getAllQuestionsForTestId(int testId);
}