package com.example.alexandrusandu.proiectandroid.Models;

import java.io.Serializable;
import java.util.Map;

public class UserModel implements Serializable {
    public String name;
    public String username;
    public String password;
    public String email;
    public String type;
    public String gender;
    public String totalScore;
    public String totalTests;

    public UserModel(String name, String username, String password, String email, String type, String gender) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.email = email;
        this.type = type;
        this.gender = gender;
        this.totalScore="0";
        this.totalTests="0";
    }
    public UserModel(Map<String, Object> userTemp){
        if(userTemp.get("name")!=null){
            this.name=(String)userTemp.get("name");
        }else{
            this.name="";
        }
        if(userTemp.get("username")!=null){
            this.username=(String)userTemp.get("username");
        }else{
            this.username="";
        }
        if(userTemp.get("password")!=null){
            this.password=(String)userTemp.get("password");
        }else{
            this.password="";
        }
        if(userTemp.get("email")!=null){
            this.email=(String)userTemp.get("email");
        }else{
            this.email="";
        }
        if(userTemp.get("type")!=null){
            this.type=(String)userTemp.get("type").toString();
        }else{
            this.type="1";
        }
        if(userTemp.get("gender")!=null){
            this.gender=(String)userTemp.get("gender");
        }else{
            this.gender="";
        }
        if(userTemp.get("totalScore")!=null){
            this.totalScore=(String)userTemp.get("totalScore");
        }else{
            this.totalScore="0";
        }
        if(userTemp.get("totalTests")!=null){
            this.totalTests=(String)userTemp.get("totalTests");
        }else{
            this.totalTests="0";
        }
    }

    public float getAverage(){
        if(Float.parseFloat(totalScore)==0||Float.parseFloat(totalTests)==0){
            return 1;
        }
        try {
            return (Float.parseFloat(totalScore) / Float.parseFloat(totalTests));
        }catch(Exception e){
            return 1;
        }
    }

}
