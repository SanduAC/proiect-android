package com.example.alexandrusandu.proiectandroid.Database.Daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.alexandrusandu.proiectandroid.Database.Models.Test;
import com.example.alexandrusandu.proiectandroid.Database.Models.TestTaken;

import java.util.List;

@Dao
public interface TestCreatedDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Test test);

    @Query("DELETE FROM tests")
    void deleteAll();

    @Query("DELETE FROM tests WHERE id=:testId")
    void deleteTest(int testId);

    @Query("SELECT * from tests")
    List<Test> getAllTestsCreated();

    @Query("SELECT * from tests WHERE createdById=:createdById")
    List<Test> getAllTestsCreatedBy(int createdById);
}