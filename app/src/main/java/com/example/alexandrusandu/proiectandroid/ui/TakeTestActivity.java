package com.example.alexandrusandu.proiectandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alexandrusandu.proiectandroid.Database.AppDatabase;
import com.example.alexandrusandu.proiectandroid.Database.Models.TestTaken;
import com.example.alexandrusandu.proiectandroid.Models.QuestionsModel;
import com.example.alexandrusandu.proiectandroid.Models.TestModel;
import com.example.alexandrusandu.proiectandroid.Models.UserModel;
import com.example.alexandrusandu.proiectandroid.R;
import com.example.alexandrusandu.proiectandroid.Utils.Constants;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TakeTestActivity extends AppCompatActivity {
    TestModel currentTest = null;
    int questionIndex = 0;
    QuestionsModel currentQuestion;
    int correctAnswers = 0;
    UserModel userModel = null;
    List<Boolean> answers=new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_test);
        getCurrentTest();
        loadQuestion();
        setViews();
        setControls();
    }

    private void setControls() {
        findViewById(R.id.answer1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(currentQuestion.correctAnswer) == 1) {
                    ((Button) findViewById(R.id.answer1)).setBackground(getResources().getDrawable(R.drawable.button_background_green_round_corners));
                    correctAnswers += 1;
                    answers.add(true);
                } else {
                    ((Button) findViewById(R.id.answer1)).setBackground(getResources().getDrawable(R.drawable.button_background_red_round_corners));
                    answers.add(false);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        questionIndex += 1;
                        if (questionIndex == currentTest.questions.size()) {
                            onTestCompletition();
                            return;
                        }
                        loadQuestion();
                        setViews();
                    }
                }, 1000);
            }
        });
        findViewById(R.id.answer2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(currentQuestion.correctAnswer) == 2) {
                    ((Button) findViewById(R.id.answer2)).setBackground(getResources().getDrawable(R.drawable.button_background_green_round_corners));
                    correctAnswers += 1;
                    answers.add(true);
                } else {
                    ((Button) findViewById(R.id.answer2)).setBackground(getResources().getDrawable(R.drawable.button_background_red_round_corners));
                    answers.add(false);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        questionIndex += 1;
                        if (questionIndex == currentTest.questions.size()) {
                            onTestCompletition();
                            return;
                        }
                        loadQuestion();
                        setViews();
                    }
                }, 1000);
            }
        });
        findViewById(R.id.answer3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(currentQuestion.correctAnswer) == 3) {
                    ((Button) findViewById(R.id.answer3)).setBackground(getResources().getDrawable(R.drawable.button_background_green_round_corners));
                    correctAnswers += 1;
                    answers.add(true);
                } else {
                    ((Button) findViewById(R.id.answer3)).setBackground(getResources().getDrawable(R.drawable.button_background_red_round_corners));
                    answers.add(false);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        questionIndex += 1;
                        if (questionIndex == currentTest.questions.size()) {
                            onTestCompletition();
                            return;
                        }
                        loadQuestion();
                        setViews();
                    }
                }, 1000);
            }
        });
        findViewById(R.id.answer4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(currentQuestion.correctAnswer) == 4) {
                    ((Button) findViewById(R.id.answer4)).setBackground(getResources().getDrawable(R.drawable.button_background_green_round_corners));
                    correctAnswers += 1;
                    answers.add(true);
                } else {
                    ((Button) findViewById(R.id.answer4)).setBackground(getResources().getDrawable(R.drawable.button_background_red_round_corners));
                    answers.add(false);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        questionIndex += 1;
                        if (questionIndex == currentTest.questions.size()) {
                            onTestCompletition();
                            return;
                        }
                        loadQuestion();
                        setViews();
                    }
                }, 1000);
            }
        });
    }

    private void onTestCompletition() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> usermap = new HashMap<>();
        usermap.put("username", userModel.username);
        usermap.put("password", userModel.password);
        usermap.put("type", userModel.type);
        usermap.put("totalScore", String.valueOf((Integer.parseInt(userModel.totalScore))+correctAnswers));
        usermap.put("totalTests", String.valueOf((Integer.parseInt(userModel.totalTests))+1));
        db.collection("users").document(userModel.username).set(usermap).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(TakeTestActivity.this, "Successfully completed the test", Toast.LENGTH_LONG).show();
                saveTestToLocalDatabase();
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(TakeTestActivity.this, "FAILED", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void saveTestToLocalDatabase() {
        TestTaken testTaken=new TestTaken(currentTest.nume, currentTest.questions.get(0).question, answers.get(0), currentTest.questions.get(1).question,  answers.get(1),currentTest.questions.get(2).question,  answers.get(2),currentTest.questions.get(3).question,  answers.get(3), currentTest.questions.get(4).question,  answers.get(4), AppDatabase.getDatabase(TakeTestActivity.this).userDao().getAllUsers().get(0).id);
        AppDatabase.getDatabase(TakeTestActivity.this).testDao().insert(testTaken);
        shouldExit=true;
        TakeTestActivity.this.onBackPressed();
    }

    private void loadQuestion() {
        currentQuestion = currentTest.questions.get(questionIndex);
    }

    private void getCurrentTest() {
        if (getIntent().getSerializableExtra(Constants.TEST_ARGUMENT) != null) {
            currentTest = (TestModel) getIntent().getSerializableExtra(Constants.TEST_ARGUMENT);
            userModel = (UserModel) getIntent().getSerializableExtra(Constants.USER_ARGUMENT);
        } else {
            Toast.makeText(TakeTestActivity.this, "Could not load TEST", Toast.LENGTH_SHORT).show();
        }
    }

    private void setViews() {
        ((TextView) findViewById(R.id.test_name)).setText("Take test: " + currentTest.nume);
        ((TextView) findViewById(R.id.question)).setText(currentQuestion.question);
        ((Button) findViewById(R.id.answer1)).setText(currentQuestion.a1);
        ((Button) findViewById(R.id.answer2)).setText(currentQuestion.a2);
        ((Button) findViewById(R.id.answer3)).setText(currentQuestion.a3);
        ((Button) findViewById(R.id.answer4)).setText(currentQuestion.a4);
        ((Button) findViewById(R.id.answer1)).setBackground(getResources().getDrawable(R.drawable.button_background_whit_round_corners));
        ((Button) findViewById(R.id.answer2)).setBackground(getResources().getDrawable(R.drawable.button_background_whit_round_corners));
        ((Button) findViewById(R.id.answer3)).setBackground(getResources().getDrawable(R.drawable.button_background_whit_round_corners));
        ((Button) findViewById(R.id.answer4)).setBackground(getResources().getDrawable(R.drawable.button_background_whit_round_corners));
    }

    boolean shouldExit = false;

    @Override
    public void onBackPressed() {
        if (shouldExit) {
            super.onBackPressed();
        } else {
            shouldExit = true;
            Toast.makeText(this, "Are you sure you want to exit the test?", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    shouldExit = false;
                }
            }, 2000);
        }
    }
}
