package com.example.alexandrusandu.proiectandroid.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alexandrusandu.proiectandroid.Database.Models.Test;
import com.example.alexandrusandu.proiectandroid.Database.Models.TestTaken;
import com.example.alexandrusandu.proiectandroid.Database.Models.User;
import com.example.alexandrusandu.proiectandroid.R;
import com.example.alexandrusandu.proiectandroid.ui.ManageTestsActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TestsRecyclerAdapterForManageForStudent extends RecyclerView.Adapter<TestsRecyclerAdapterForManageForStudent.ViewHolder> {

    List<TestTaken> tests;
    User user;
    ManageTestsActivity manageTestsActivity;

    public TestsRecyclerAdapterForManageForStudent(ManageTestsActivity manageTestsActivity, List<TestTaken> tests, User user) {
        this.tests = tests;
        this.user = user;
        this.manageTestsActivity = manageTestsActivity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.test_taken_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        final TestsRecyclerAdapterForManageForStudent.ViewHolder vh = new TestsRecyclerAdapterForManageForStudent.ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        if (position == tests.size()-1) {
            viewHolder.endDivider.setVisibility(View.VISIBLE);
        }

        final TestTaken currentTest = tests.get(position);
        if(user.name.equals("")){
            viewHolder.studentName.setText(user.username);
        }else{
            viewHolder.studentName.setText(user.name);
        }
        int counter=0;
        if(currentTest.isCorrect1()){
            counter+=1;
        }
        if(currentTest.isCorrect2()){
            counter+=1;
        }
        if(currentTest.isCorrect3()){
            counter+=1;
        }
        if(currentTest.isCorrect4()){
            counter+=1;
        }
        if(currentTest.isCorrect4()){
            counter+=1;
        }
        viewHolder.score.setText(counter+"/5");
    }

    @Override
    public int getItemCount() {
        return tests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView studentName;
        TextView score;
        View endDivider;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.nume_test);
            studentName = itemView.findViewById(R.id.student_name);
            score = itemView.findViewById(R.id.score);
            endDivider = itemView.findViewById(R.id.end_divider);
        }
    }
}
